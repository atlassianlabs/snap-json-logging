{-# LANGUAGE OverloadedStrings #-}

module Snap.Logging.Json ( initSnaplet
                         , logJson
                         ) where

import           Control.Lens
import           Control.Monad.IO.Class
import           Data.Aeson
import qualified Data.ByteString.Lazy   as BL
import           Snap.Logging.Json.Data
import qualified Snap.Snaplet           as SS
import           System.IO              (Handle, hPutStrLn)

initSnaplet :: Handle -> SS.SnapletInit b JsonLogging
initSnaplet handle = SS.makeSnaplet "json-logging" "JSON Logging" Nothing $ do
  -- TODO validate Write or Append access
  return JsonLogging { _jsonLoggingHandle = handle }

logJson :: ToJSON a => a -> SS.Handler b JsonLogging ()
logJson a = do
  handle <- view (SS.snapletValue . jsonLoggingHandle) <$> SS.getSnapletState
  liftIO $ BL.hPut handle (encode a `BL.append` "\n")
