{-# LANGUAGE TemplateHaskell #-}

module Snap.Logging.Json.Data ( JsonLogging(..)
                              , jsonLoggingHandle
                              , HasJsonLogging(..)
                              ) where

import           Control.Lens
import           System.IO    (Handle)

data JsonLogging = JsonLogging
  { _jsonLoggingHandle :: Handle
  }
makeLenses ''JsonLogging

class HasJsonLogging m where
  getJsonLogging :: m JsonLogging
