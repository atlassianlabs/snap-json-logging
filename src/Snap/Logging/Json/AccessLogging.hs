{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Snap.Logging.Json.AccessLogging (logAccess
                                       ) where

import           Control.Monad.IO.Class (liftIO)
import           Data.Aeson
import           Data.Aeson.TH
import           Data.ByteString
import           Data.Text              as T
import qualified Data.Text.Encoding     as TE
import           Data.Time.Clock
import           GHC.Generics
import           Snap.Core
import           Snap.Logging.Json
import           Snap.Logging.Json.Data
import           Snap.Snaplet
import qualified Snap.Types.Headers     as H

data AccessLogLine = AccessLogLine
  { host       :: Text
  , httpMethod :: Method
  , reqUri     :: Text
  , httpVer    :: Text
  , status     :: Int
  , referer    :: Maybe Text
  , userAgent  :: Maybe Text
  , time       :: UTCTime
  }
  deriving Show

instance ToJSON Method where
  toJSON = String . T.pack . show

$(deriveToJSON defaultOptions ''AccessLogLine)

logAccess :: Handler b JsonLogging ()
logAccess = do
  req <- getRequest
  resp <- getResponse
  now <- liftIO getCurrentTime
  logJson $ logLine req resp now

logLine :: Request -> Response -> UTCTime -> AccessLogLine
logLine req resp time' = AccessLogLine
  { host = TE.decodeUtf8 $ rqClientAddr req
  , httpMethod = rqMethod req
  , reqUri = TE.decodeUtf8 $ rqURI req
  , httpVer = ver
  , status = rspStatus resp
  , referer = TE.decodeUtf8 <$> H.lookup "referer" headers
  , userAgent = TE.decodeUtf8 <$> H.lookup "user-agent" headers
  , time = time'
  }
  where
    headers = rqHeaders req
    (v, v') = rqVersion req
    ver = T.pack $ "HTTP/" ++ show v ++ "." ++ show v'
